
function PriorityQueue(fnCompare) {
    this._items = new Array();
    if (typeof fnCompare == "function") {
        this._compare = fnCompare;
    }
}

PriorityQueue.prototype = {
        
    _compare : function (oValue1, oValue2) {
        if (oValue1 < oValue2) {
            return -1;
        } else if (oValue1 > oValue2) {
            return 1;
        } else {
            return 0;
        }
    },
    
    //method retrieves the next item in the queue.
    /**
     * The get()method uses the array’s shift()method to remove
     * and return the first item in the array (if the array is empty, shift() returns null). 
     * */
    get : function() {
        return this._items.shift();
    },
    
    //returns an item in a given position
    item : function (iIndex) {
        return this._items[iIndex];
    },
    
    //gets the next item in the queue without actually removing it (just a preview of the next item).
    //peek()just gets the first item in the array using the 0 index, which returns the value without removing it
    peek : function () {
        return this._items[0];
    },
    
    prioritize : function () {
        this._items.sort(this._compare);
    },
    
    //is responsible for adding a new value to the queue.
    /**
     * The put()method is the one that adds a value to the queue.
     * It first adds the value to the array and then calls prioritize().
     * */
    put : function (oValue) {
        this._items.push(oValue);
        this.prioritize();
    }, 
    
    /**
     * The remove() method uses a forloop to search for a specific value in the array.
     * The value is determined by using the identically equal operator (===) to ensure that types aren’t converted
     * when making the comparison. If the matching value is found,
     * it is removed from the array using the splice() method and a value of trueis returned;
     * if no matching value is found, it returns false.
     * */
    remove : function (oValue) {
        for (var i = 0; i < this._items.length; i++) {
            if (this._items[i] === oValue) {
                this._items.splice(i, 1);
                return true;
            }
        }
        return false;
    },
    
    //simply returns the number of items in the queue.
    size : function () {
        return this._items.length;
    }
};