<%@page contentType="text/plain" pageEncoding="UTF-8" import="java.net.*, java.sql.*, java.io.*, org.json.*"%><%
response.setHeader("Cache-Control","no-cache");

String[] symbols = { "MSFT", "GE" };
String symbolString = "";
String data = "";
JSONObject jsonOutput = new JSONObject();

if (symbols.length > 0) {
    //Build the symbol string
	for (String symbol: symbols)
		symbolString += symbol + "+";
    
    //take off the final +
	symbolString = symbolString.substring(0, symbolString.lastIndexOf("+"));
	
	//Assemble the URL
	URL url = new URL(String.format("http://finance.yahoo.com/d/quotes.csv?s=%s&f=snl1c1", symbolString));
	
	//And do the connection stuff
	HttpURLConnection huc = (HttpURLConnection) url.openConnection() ; 
       
    huc.setRequestMethod("GET") ; 
       
    huc.connect();
    
    if (huc.getResponseCode() == 200 || huc.getResponseCode() == 304)
    {
        byte[] buffer = new byte[4096];
        InputStream is = huc.getInputStream();
        OutputStream outputStream = new ByteArrayOutputStream();
        
        while (true)
        {
            int read = is.read(buffer);
         
            if (read == -1)
            {
                break;
            }
            
            outputStream.write(buffer, 0, read);
        }
        
        //Get the data and break it up into an array
        outputStream.close();
        data = outputStream.toString();
        data = data.replace("\"", "");
        String[] splitData = data.split("\r\n");
        
        //We didn't have an error
        jsonOutput.put("error", false);
        JSONArray jsonArray = new JSONArray();
        
        //Assemble the JSON for the stocks
        for (String stock: splitData)
        {
            String[] stockData = stock.split(",");
            JSONObject stockObject = new JSONObject();
            
            stockObject.put("symbol", stockData[0]);
            stockObject.put("companyName", stockData[1]);
            stockObject.put("lastTrade", stockData[2]);
            stockObject.put("change", stockData[3]);
            
            jsonArray.put(stockObject);
        }
        
        //And put that information into the main JSONObject
        jsonOutput.put("stocks", jsonArray);   
    }
    else
    {
        //We had an error
        jsonOutput.put("error", true);
    }
    
    //disconnect
	huc.disconnect();
}


%><%=jsonOutput.toString()%>