<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Chart-jqplog</title>
<link href="css/style.css" media="screen" rel="stylesheet">
<link href="../js/libs/jqplot/jquery.jqplot.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="../js/libs/jqplot/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/libs/jqplot/jquery.jqplot.min.js"></script>
<!-- Line charts, scatter plots and series options. -->
<script type="text/javascript" src="../js/libs/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="../js/libs/jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>

<script type="text/javascript" src="../js/libs/jqplot/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="../js/libs/jqplot/plugins/jqplot.donutRenderer.min.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    // mang mot chieu.
    var data = [ null ];
    $.jqplot('chart1', [ data ], {title : 'Line Style Default',});
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    var data = [ 3, 7, 9, 1, 4, 6, 8, 2, 5 ];
    $.jqplot('chart2', [ data ], {
      // Give the plot a title.
      title : 'Plot With Options',
      // You can specify options for all axes on the plot at once with the axesDefaults object.
      // Here, we're using a canvas renderer to draw the axis label which allows rotated text.
      axesDefaults : {
        labelRenderer : $.jqplot.CanvasAxisLabelRenderer
      },
      // An axes object holds options for all axes. Allowable axes are xaxis, x2axis, yaxis, y2axis, y3axis, ...
      // Up to 9 y axes are supported.
      axes : {
        // options for each axis are specified in seperate option objects.
        xaxis : {
          label : "X Axis",
          // Turn off "padding".  This will allow data point to lie on the edges of the grid.
          // Default padding is 1.2 and will keep all points inside the bounds of the grid.
          pad : 0
        },
        yaxis : {
          label : "Y Axis"
        }
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(
      function () {
        // Some simple loops to build up data arrays.
        var cosPoints = [];
        for ( var i = 0; i < 2 * Math.PI; i += 0.4) {
          cosPoints.push([ i, Math.cos(i) ]);
        }
        
        var sinPoints = [];
        for ( var i = 0; i < 2 * Math.PI; i += 0.4) {
          sinPoints.push([ i, 2 * Math.sin(i - .8) ]);
        }
        
        var powPoints1 = [];
        for ( var i = 0; i < 2 * Math.PI; i += 0.4) {
          powPoints1.push([ i, 2.5 + Math.pow(i / 4, 2) ]);
        }
        
        var powPoints2 = [];
        for ( var i = 0; i < 2 * Math.PI; i += 0.4) {
          powPoints2.push([ i, -2.5 - Math.pow(i / 4, 2) ]);
        }
        
        var plot3 = $.jqplot('chart3', [ cosPoints, sinPoints, powPoints1, powPoints2 ], {
          title : 'Line Style Options',
          // Series options are specified as an array of objects, one object for each series.
          series : [ {
            // Change our line width and use a diamond shaped marker.
            lineWidth : 2,
            markerOptions : {
              style : 'dimaond'
            }
          }, {
            // Don't show a line, just show markers. Make the markers 7 pixels with an 'x' style
            showLine : false,
            markerOptions : {
              size : 7,
              style : "x"
            }
          }, {
            // Use (open) circlular markers.
            markerOptions : {
              style : "circle"
            }
          }, {
            // Use a thicker, 5 pixel line and 10 pixel filled square markers.
            lineWidth : 5,
            markerOptions : {
              style : "filledSquare",
              size : 10
            }
          } ]
        });
        var data = [ ['Heavy Industry', 1],['Retail',0]];
        var plot4 = jQuery.jqplot('chart4', [data], {
          title : 'Pie Chart',
          seriesDefaults : {
            // Make this a pie chart.
            renderer : jQuery.jqplot.PieRenderer,
            rendererOptions : {
              // Put data labels on the pie slices.
              // By default, labels show the percentage of the slice.
              showDataLabels : true
            }
          },
          legend : {
            show : true,
            location : 'e'
          }
        });
      });
</script>
</head>
<body>
		<!--
  The most basic jqPlot chart takes a series of data and plots a line.
  No options need to be supplied. Data is passed in as an array of series.
  A series can be either an array of y values or an array of [x,y] data pairs.
  If y values only, x values are assigned like 1, 2, 3, ... Note, for this plot you don't need any plugins.
-->
		<div class="container">
				<div id="chart1" style="height: 300px; width: 500px; position: relative;" class="jqplot-target"></div>
		</div>
		<!--
  The following plot uses a number of options to set the title, add axis labels,
  and shows how to use the canvasAxisLabelRenderer plugin to provide rotated axis labels.
 -->
		<div class="container">
				<div id="chart2" style="height: 300px; width: 500px; position: relative;" class="jqplot-target"></div>
		</div>
		<!-- There are numerous line style options to control how the lines and markers are displayed. -->
		<div class="container">
				<div id="chart3" style="height: 300px; width: 500px; position: relative;" class="jqplot-target"></div>
		</div>
		<div class="container">
				<div id="chart4" style="height: 300px; width: 500px; position: relative;" class="jqplot-target"></div>
		</div>
</body>
</html>