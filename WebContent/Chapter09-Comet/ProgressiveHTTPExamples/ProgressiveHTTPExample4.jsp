<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Progressive HTTP Example 4</title>
         <script type="text/javascript">
        //<![CDATA[ 
        
        var iTimeoutId = null;
        
        function heartbeat() {
            clearTimeout(iTimeoutId);            
            iTimeoutId = setTimeout(resetConnection, 10000);
        }
        
        function resetConnection() {
            frames["connection"].location.replace("ProgressiveHTTPExample4Connection.jsp?t=" + (new Date()).getTime());
            heartbeat();
        }
        
        function modifiedAt(sDateTime) {
            document.getElementById("divStatus").innerHTML = "Modified at " + sDateTime;
        }
        
        window.onload = resetConnection;

        //]]>
        </script>         
    </head>
    <body>  
        <div id="divStatus">Waiting for first message...</div>
        <iframe src="about:blank" name="connection"></iframe>
    </body>
</html>
