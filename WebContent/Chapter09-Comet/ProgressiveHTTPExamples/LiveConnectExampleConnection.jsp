<%@page contentType="text/html" pageEncoding="UTF-8" buffer="none" import="java.io.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
     <title>Live Connect Example</title>
         </head>
<body>
    <%
    //get initial modification time
    File file = new File(application.getRealPath("details.txt"));
    long modified = file.lastModified();
    long lastModified = modified;
    
    //check every so often to see if it's changed
    while (true) {
%>
    heartbeat();
    <%
        response.flushBuffer();
                
        //sleep for a second
        Thread.sleep(1000);
        
        //get the modification time
        modified = file.lastModified();
        
        //if it's different, output the event
        if (modified != lastModified) {
%>
    modifiedAt("<%=modified%>");
    <%
            response.flushBuffer();
            
            lastModified = modified;
            Thread.sleep(1000);
        }
    }
%>
</body>
</html>
<%-- JSP throws an error if there's any content after this point --%>
