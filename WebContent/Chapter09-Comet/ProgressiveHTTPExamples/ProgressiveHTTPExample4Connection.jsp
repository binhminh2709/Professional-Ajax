<%@page contentType="text/html" pageEncoding="UTF-8" buffer="none" import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Progressive HTTP Example 4 Connection</title>
</head>
<body>
    <%
   //send the output to this point
    response.flushBuffer();
    
    //get initial modification time
    File file = new File(application.getRealPath("details.txt"));
    long modified = file.lastModified();
    long lastModified = modified;
    
    //check every so often to see if it's changed
    while(true) {
%>
    <script type="text/javascript">
        parent.heartbeat();
        </script>
    <%             
        //send the output  
        response.flushBuffer();
        
        //sleep for a second
        Thread.sleep(1000);
        
        //get the modification time
        modified = file.lastModified();
        
        //if it's different, output the event
        if (modified != lastModified)
        {            
%>
    <script type="text/javascript">
                parent.modifiedAt("<%=modified %>");
                </script>
    <%
        //send the output
                response.flushBuffer();
                
                //update
                lastModified = modified;
                
                //sleep for a second
                Thread.sleep(1000);
            }
        }
    %>
    <%-- JSP throws an error if there's anything after this pont --%>
</body>
</html>