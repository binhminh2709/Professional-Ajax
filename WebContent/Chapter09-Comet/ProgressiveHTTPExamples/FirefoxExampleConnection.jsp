<%@page contentType="text/javascript" pageEncoding="UTF-8" buffer="none" import="java.io.*" %>
<%

    //get initial modification time
    File file = new File(application.getRealPath("details.txt"));
    long modified = file.lastModified();
    long lastModified = modified;
    
    //check every so often to see if it's changed
    while(true) {
        %>;heartbeat()<%
        response.flushBuffer();
        
        //sleep for a second
        Thread.sleep(1000);
        
        //get file modification time
        modified = file.lastModified();
        
        //check to see if it's different
        if (modified != lastModified){
            %>;modifiedAt("<%=modified%>")<%
            response.flushBuffer();
            
            
            //update
            lastModified = modified;
            
            /**sleep for a second*/
            Thread.sleep(1000);
        }
    }
%><%-- JSP throws an error if there's any content after this point --%>
