<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Customer Account Information</title>
<script type="text/javascript">
  function requestCustomerInfo() {
    var sId = document.getElementById("txtCustomerId").value;
/**
  window.top.frames, lấy ra frame với tên hiddenFrame
  <frame name="hiddenFrame" src="about:blank" noresize="noresize" />
  hiển thị thông tin vào hiddenFrame
*/
    
/**
  The first step in this function is to retrieve the customer identification number from the textbox.
To do so, document.getElementById()is called with the textbox ID, “txtCustomerId”, and the value property is retrieved.
(The valueproperty holds the text that is inside the textbox.)
  
  Then, this ID is added to the string “GetCustomerData.php?id=”to create the full URL.
The second line creates the URL and assigns it to the hidden frame. To get a reference to the hidden frame,
you first need to access the topmost window of the browser using the topobject.
That object has a framesarray, within which you can find the hidden frame. Since each frame is just another window object,
you can set its location to the desired URL.
*/
    top.frames["hiddenFrame"].location = "GetCustomerData.jsp?id=" + sId;
  }

  /**
    Hiển thị thông tin của dữ liệu truyền vào
    The customer info string (sText) is assigned into the innerHTMLproperty of the <div/> element.
    Using innerHTMLmakes it possible to embed HTML into the string for formatting purposes.
    This completes the code for the main display page.
  */
  function displayCustomerInfo(sText) {
    var divCustomerInfo = document.getElementById("divCustomerInfo");
    divCustomerInfo.innerHTML = sText;
  }
</script>
</head>
<body>
  <p>Enter customer ID number to retrieve information:</p>
  <p>Customer ID: <input type="text" id="txtCustomerId" value="" /></p>
  <p><input type="button" value="Get Customer Info" onclick="requestCustomerInfo()" /></p>
  <div id="divCustomerInfo"></div>
</body>
</html>
