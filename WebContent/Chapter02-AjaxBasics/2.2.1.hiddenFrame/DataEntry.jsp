<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Create New Customer</title>
  <script type="text/javascript">
    function saveResult(sMessage) {
      var divStatus = document.getElementById("divStatus");
      divStatus.innerHTML = "Request completed: " + sMessage;
    }
  </script>
</head>
<body>
  <form method="post" action="SaveCustomer.jsp" target="hiddenFrame">
    <p>Enter customer information to be saved:</p>
    <table>
      <tr><td>Customer Name:</td><td><input type="text" name="txtName" value="" /></td></tr>
      <tr><td>Address:</td><td><input type="text" name="txtAddress" value="" /></td></tr>
      <tr><td>City:</td><td><input type="text" name="txtCity" value="" /></td></tr>
      <tr><td>State:</td><td><input type="text" name="txtState" value="" /></td></tr>
      <tr><td>Zip Code:</td><td><input type="text" name="txtZipCode" value="" /></td></tr>
      <tr><td>Phone:</td><td><input type="text" name="txtPhone" value="" /></td></tr>
      <tr><td>E-mail:</td><td><input type="text" name="txtEmail" value="" /></td></tr>
    </table>
    <p>
    </p>
    <p>
      <input type="submit" value="Save Customer Info" />
    </p>
  </form>
  <div id="divStatus"></div>
</body>
</html>
