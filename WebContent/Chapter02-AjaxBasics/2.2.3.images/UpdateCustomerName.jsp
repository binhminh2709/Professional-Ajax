<%@page import="com.utils.Constrant"%>
<%@page import="java.awt.*"%>
<%@page import="java.awt.image.*"%>
<%@page import="sun.awt.image.codec.*"%>
<%@page import="java.sql.*"%>
<%@page contentType="image/jpeg"%>
<%
  //width of the image
  int imageWidth = 1;
  
  //get information (replace apostrophes with double apostrophes to prevent SQL injection attacks)
  String name = request.getParameter("name").replace("'", "''");
  String strId = request.getParameter("id");
  
  //check for valid ID
  int id = -1;
  
  try {
    id = Integer.parseInt(strId);
  } catch (Exception ex) {
    imageWidth = 2;
  }
  
  //if the ID is valid...
  if (imageWidth == 1) {
    
    //database information
    String dbservername = "localhost";
    String dbname = Constrant.DB_NAME;
    String username = Constrant.DB_USERNAME;
    String password = Constrant.DB_PASSWORD;
    String url = "jdbc:mysql://" + dbservername + "/" + dbname + "?user=" + username + "&password=" + password;
    
    try {
      
      //create instance of the MySQL driver
      Class.forName("com.mysql.jdbc.Driver").newInstance();
      
      //create database connection
      Connection conn = DriverManager.getConnection(url);
      
      //construct the query
      StringBuffer buffer = new StringBuffer();
      buffer.append("Update Customers set Name='");
      buffer.append(name);
      buffer.append("' where CustomerId=");
      buffer.append(id);
      buffer.append(";");
      String sql = buffer.toString();
      
      //execute it
      Statement stmt = conn.createStatement();
      if (stmt.executeUpdate(sql) <= 0) {
        imageWidth = 3;
      }
      
    } catch (Exception ex) {
      imageWidth = 3;
    }
  }
  
  //create the image
  BufferedImage image = new BufferedImage(imageWidth, 1, BufferedImage.TYPE_INT_RGB);
  Graphics2D g = (Graphics2D) image.getGraphics();
  g.setColor(Color.white);
  g.fillRect(0, 0, 1, 1);
  g.dispose();
  
  //output it
  ServletOutputStream output = response.getOutputStream();
  JPEGImageEncoderImpl encoder = new JPEGImageEncoderImpl(output);
  encoder.encode(image);
%>