<%@page contentType="image/gif"%>
<%
  //Note that this redirect to an image, regardless of your server-side language,
  //should be done after other processing has occurred. 
  response.sendRedirect("pixel.gif");
%>