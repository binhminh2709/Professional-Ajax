<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Image Example 1</title>
<script type="text/javascript">
	
/**
Each request to and response from the server contains cookie information,
regardless of what type of resource is being requested.
This means that setting the srcattribute of an image brings back updated cookie information from the server.
Assuming that an onloadevent handler has been assigned to the image,
this is where you can retrieve the new cookie information.
The following function can be used to access specific cookie values
*/
  function getCookie (sName) {
    var sRE = "(?:; )?" + encodeURIComponent(sName) + "=([^;]*);?";
    //alert(sRE);
    var oRE = new RegExp(sRE);
    //alert(oRE);
    if (oRE.test(document.cookie)) {
      return decodeURIComponent(RegExp["$1"]);
    } else {
      return null;
    }
  }
/**
* This function looks through the document.cookie property,
* which is a series of name-value pairs representing each cookie accessible by the page.
* The pairs are URL-encoded and separated by semicolons (;),
* which is why a regular expression is used to extract the appropriate value.
* If the cookie with the given name doesn’t exist, the function returns null.
*/
  function deleteCookie (sName) {
    document.cookie = encodeURIComponent(sName) + "=0; expires=Thu, 1 Jan 1970 00:00:00 UTC; path=/";
  }
/**
 Setting the expiration date of a cookie to some date that has already passed effectively removes the
 cookie from the client machine. This function uses January 1, 1970, in GMT format to delete the cookie
 (the JavaScript Date object has a toGMTString() method that can be used to get this format for any
 date). The path argument is important as well, as it ensures that the cookie is removed for every location
 on the domain, not just the current page.
 */

  function requestCustomerInfo() {
    var sId = document.getElementById("txtCustomerId").value;
    var oImg = new Image();
    oImg.onload = function () {
      displayCustomerInfo(getCookie("info"));
      deleteCookie("info");
    };
    oImg.onerror = function () {
      displayCustomerInfo("An error occurred while processing the request.");
    };
    oImg.src = "GetCustomerData.jsp?id=" + sId;
  }

  function displayCustomerInfo (sText) {
    var divCustomerInfo = document.getElementById("divCustomerInfo");
    divCustomerInfo.innerHTML = sText;
  }
</script>
</head>
<body>
  <p>Enter customer ID number to retrieve information:</p>
  <p>Customer ID: <input type="text" id="txtCustomerId" value="" /></p>
  <p><input type="button" value="Get Customer Info" onclick="requestCustomerInfo()" /></p>
  <div id="divCustomerInfo"></div>
</body>
</html>
