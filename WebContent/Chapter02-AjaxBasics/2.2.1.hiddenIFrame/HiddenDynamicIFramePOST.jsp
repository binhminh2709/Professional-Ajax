<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Hidden Dynamic IFrame POST Example</title>
<script type="text/javascript">
  var oIFrame = null;
  /**
The second way to use hidden iframes is to create them dynamically using JavaScript.
This can get a little bit tricky because not all browsers implement iframes in the same way,
so it helps to simply go step by step in creating a hidden iframe.
  */
  function createIFrame() {
    var oIFrameElement = document.createElement("iframe");
    oIFrameElement.style.display = "none";
    oIFrameElement.name = "hiddenFrame";
    oIFrameElement.id = "hiddenFrame";
    document.body.appendChild(oIFrameElement);
    oIFrame = frames["hiddenFrame"];
  }

  function checkIFrame() {
    if (!oIFrame) {
      createIFrame();
    }
    
    //takes a cycle for some browers to recognize the new element
    setTimeout(function() {
      oIFrame.location = "ProxyForm.jsp";
    }, 10);
  }

  function createInputField(oHiddenForm, sName, sValue) {
    oHidden = oIFrame.document.createElement("input");
    oHidden.type = "hidden";
    oHidden.name = sName;
    oHidden.value = sValue;
    oHiddenForm.appendChild(oHidden);
  }

  function formReady() {
    //get your reference to the form
    var oForm = document.forms[0];
    var oHiddenForm = oIFrame.document.forms[0];
    
    //iterate over each element in the form
    for ( var i = 0; i < oForm.elements.length; i++) {
      
      //get reference to the field
      var oField = oForm.elements[i];
      
      //different behavior based on the type of field
      switch (oField.type) {
        
        //buttons - we don't care
        case "button":
        case "submit":
        case "reset":
          break;
        
        //checkboxes/radio buttons - only return the value if the control is checked.
        case "radio":
        case "checkbox":
          if (!oField.checked) {
            break;
          }
          
          //text/hidden/password all return the value
        case "text":
        case "hidden":
        case "password":
          createInputField(oHiddenForm, oField.name, oField.value);
          break;
        
        //everything else
        default:
          switch (oField.tagName.toLowerCase()) {
            case "select":
              createInputField(oHiddenForm, oField.name, oField.options[oField.selectedIndex].value);
              break;
            default:
              createInputField(oHiddenForm, oField.name, oField.value);
          }
      }
    }
    
    oHiddenForm.action = oForm.action;
    oHiddenForm.submit();
  };
  
  function saveResult(sMessage) {
    var divStatus = document.getElementById("divStatus");
    divStatus.innerHTML = "Request completed: " + sMessage;
  }
</script>
</head>
<body>
  <form method="post" action="SaveCustomer.jsp" onsubmit="checkIFrame(); return false">
    <p>Enter customer information to be saved:</p>
    <table>
      <tr><td>Customer Name:</td><td><input type="text" name="txtName" value="" /></td></tr>
      <tr><td>Address:</td><td><input type="text" name="txtAddress" value="" /></td></tr>
      <tr><td>City:</td><td><input type="text" name="txtCity" value="" /></td></tr>
      <tr><td>State:</td><td><input type="text" name="txtState" value="" /></td></tr>
      <tr><td>Zip Code:</td><td><input type="text" name="txtZipCode" value="" /></td></tr>
      <tr><td>Phone:</td><td><input type="text" name="txtPhone" value="" /></td></tr>
      <tr><td>E-mail:</td><td><input type="text" name="txtEmail" value="" /></td></tr>
    </table>
    <p><input type="submit" value="Save Customer Info" /></p>
  </form>
  <div id="divStatus"></div>
</body>
</html>
