<%@page contentType="text/xml" pageEncoding="UTF-8" import="java.net.*, java.io.*"%>
<%
    response.setHeader("Cache-Control", "no-cache");
    if (request.getParameter("url") != null) {
        URL url = new URL(request.getParameter("url"));
        
        HttpURLConnection huc = (HttpURLConnection) url.openConnection();
        
        huc.setRequestMethod("GET");
        
        huc.connect();
        
        if (huc.getResponseCode() == 200 || huc.getResponseCode() == 304) {
            byte[] buffer = new byte[4096];
            InputStream is = huc.getInputStream();
            OutputStream outputStream = new ByteArrayOutputStream();
            
            while (true) {
                int read = is.read(buffer);
                
                if (read == -1) {
                    break;
                }
                
                outputStream.write(buffer, 0, read);
            }
            
            outputStream.close();
            
            out.println(outputStream.toString());
        }
        
        huc.disconnect();
    } else {
        response.sendError(500);
    }
%>