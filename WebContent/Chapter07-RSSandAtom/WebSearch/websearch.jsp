<%@page contentType="text/xml" pageEncoding="UTF-8" import="java.net.*, java.io.*"%>
<%
    response.setHeader("Cache-Control", "no-cache");
    if (request.getParameter("search") != null) {
        String searchTerm = request.getParameter("search");
        URL url = new URL(String.format("http://search.msn.com/results.aspx?q=%s&format=rss",
                URLEncoder.encode(searchTerm, "UTF-8")));
        
        HttpURLConnection huc = (HttpURLConnection) url.openConnection();
        
        huc.setRequestMethod("GET");
        
        huc.connect();
        
        if (huc.getResponseCode() == 200 || huc.getResponseCode() == 304) {
            byte[] buffer = new byte[4096];
            InputStream is = huc.getInputStream();
            OutputStream outputStream = new ByteArrayOutputStream();
            
            while (true) {
                int read = is.read(buffer);
                
                if (read == -1) {
                    break;
                }
                
                outputStream.write(buffer, 0, read);
            }
            
            outputStream.close();
            
            out.println(outputStream.toString());
        }
        
        huc.disconnect();
    } else {
        response.sendError(500);
    }
%>