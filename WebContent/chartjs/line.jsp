<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Visualize Data Beautifully Using JS Charts</title>
<link href="css/style.css" media="screen" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/Chart.min.js"></script>
</head>
<body>
  <div class="container">
    <header>
      <h1>Visualize Data Beautifully Using JS Charts</h1>
    </header>

    <nav>
      <a class="active" href="line.jsp">Line Chart</a> <a href="bar.jsp">Bar Chart</a> <a href="radar.jsp">Radar
        Chart</a> <a href="polar-area.jsp">Polar Area Chart</a> <a href="pie.jsp">Pie Chart</a> <a href="doughnut.jsp">Doughnut
        Chart</a>
    </nav>
    <input type="button" value="Xem ChartLine" onclick="viewChartLine();">

    <!-- HTML5 Canvas -->
    <canvas id="canvas" height="450" width="1080"></canvas>
  </div>

  <script type="text/javascript">
      var LineChart = {
        labels : [],
        datasets : [ {
          fillColor : "rgba(151,249,190,0.5)",
          strokeColor : "rgba(255,255,255,1)",
          pointColor : "rgba(220,220,220,1)",
          pointStrokeColor : "#fff",
          data : []
        }, {
          fillColor : "rgba(252,147,65,0.5)",
          strokeColor : "rgba(255,255,255,1)",
          pointColor : "rgba(173,173,173,1)",
          pointStrokeColor : "#fff",
          data : []
        } ]
      
      }

      var datas = [ "Ruby", "jQuery", "Java", "ASP.Net", "PHP", "C#", "JavaScript", "HTML" ];
      var dataNumber1 = [ 10, 20, 30, 40, 50, 60, 70, 80 ];
      var dataNumber2 = [ 28, 68, 40, 19, 96, 80, 30, 50 ];
      
      function viewChartLine() {
        LineChart.labels = datas;
        LineChart.datasets[0].data = dataNumber1;
        LineChart.datasets[1].data = dataNumber2;
        
        /**
        bạn có thể thêm vào một số style mà bạn muốn trong mỗi biểu đồ như: mầu chữ, kích thước font chữ v.v...
        */
        var myLineChart = new Chart(document.getElementById("canvas").getContext("2d")).Line(LineChart, {
          scaleFontSize : 13,
          scaleFontColor : "#ffa45e"
        });
      }
    </script>
</body>
</html>
