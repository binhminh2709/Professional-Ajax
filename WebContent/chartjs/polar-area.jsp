<!DOCTYPE html>
<html lang="en">
  <head>
	   <meta charset="utf-8">
       <title>Visualize Data Beautifully Using JS Charts</title>
      <link href="css/style.css" media="screen" rel="stylesheet">
	  	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	  <script type="text/javascript" src="js/Chart.min.js"></script>
</head> 

	<body>
	<div class="container">
	<header>
		<h1>Visualize Data Beautifully Using JS Charts</h1>	
	</header>
	
	<nav>
			<a href="line.jsp">Line Chart</a>
			<a href="bar.jsp">Bar Chart</a>
			<a href="radar.jsp">Radar Chart</a>
			<a class="active" href="polar-area.jsp">Polar Area Chart</a>
			<a href="pie.jsp">Pie Chart</a>
			<a href="doughnut.jsp">Doughnut Chart</a>
	</nav>

		<canvas id="canvas" height="400" width="1080"></canvas>

</div>
	<script>
	var PolarChart = [
			{
			
				value : Math.random(),
				color: "#fac59c"
			},
			{
				value : Math.random(),
				color: "#c9fadd"
			},
			{
				value : Math.random(),
				color: "#a2efff"
			},
			{
				value : Math.random(),
				color: "#e6d1ff"
			},
			{
				value : Math.random(),
				color: "#ffd1f9"
			},
			{
				value : Math.random(),
				color: "#ffd1d1"
			}
		];
	var myPolarAreaChart = new Chart(document.getElementById("canvas").getContext("2d")).PolarArea(PolarChart, {scaleFontSize : 13, 	scaleFontColor : "#ffa45e"});
	</script>

</body>
</html>


