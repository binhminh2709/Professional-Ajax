<!DOCTYPE html>
<html lang="en">
  <head>
	   <meta charset="utf-8">
       <title>Visualize Data Beautifully Using JS Charts</title>
      <link href="css/style.css" media="screen" rel="stylesheet">
	  	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	  <script type="text/javascript" src="js/Chart.min.js"></script>
</head> 

	<body>
	<div class="container">
	<header>
		<h1>Visualize Data Beautifully Using JS Charts</h1>	
	</header>
	
	<nav>
			<a href="line.jsp">Line Chart</a>
			<a href="bar.jsp">Bar Chart</a>
			<a href="radar.jsp">Radar Chart</a>
			<a href="polar-area.jsp">Polar Area Chart</a>
			<a class="active" href="pie.jsp">Pie Chart</a>
			<a href="doughnut.jsp">Doughnut Chart</a>
	</nav>
	
	
		<canvas id="canvas" height="450" width="1080"></canvas>
</div>

	<script>

		var PieChart = [
				{
					value: 40,
					color:"#fcc79e"
				},
				{
					value : 30,
					color : "#beefd2"
				},
				{
					value : 90,
					color : "#ffddfb"
				}
			
			];

	var myPieChart = new Chart(document.getElementById("canvas").getContext("2d")).Pie(PieChart);
	
	</script>
</body>
</html>


