#SET ANSI_NULLS ON
#GO
#SET QUOTED_IDENTIFIER ON
#GO
#SET ANSI_PADDING ON
#GO
create database `ProAjax`;
CREATE TABLE `StatesAndProvinces`(
	`Id` int NOT NULL auto_increment,
	`Name` varchar(255) NOT NULL,
    CONSTRAINT `PK_StatesAndProvinces` PRIMARY KEY CLUSTERED (`Id` ASC )
)
#ON [PRIMARY]
#WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
#GO
#SET ANSI_PADDING OFF
select * from `StatesAndProvinces`