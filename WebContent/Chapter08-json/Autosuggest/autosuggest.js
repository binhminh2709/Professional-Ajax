/**
 * ❑ Typeahead:As the user is typing, the rest of the textbox fills in with the best suggestion at the time.
 * As the user continues to type, the textbox automatically adjusts its suggestion.
 * The suggested text always appears selected (highlighted). This should work no matter how fast the user types.
 * 
 * ❑ Suggestions list:Also as the user is typing, a drop-down list of other suggestions is displayed.
 * These suggestions are generated automatically while the user types so that there is no discernible delay.
 * 
 * ❑ Keyboard controls:When the suggestions are displayed, the user is able to scroll up 
 * and down the list by using the up and down arrows on the keyboard and select a suggestion.
 * Pressing Enter places the value into the textbox and hides the suggestion list.
 * The Esc key can also be used to hide the suggestions.
 * 
 * ❑ Hide suggestions:The drop-down suggestion list is smart enough to hide itself
 * whenever the textbox is not used or when the browser window is hidden.
 * */
/**
 * Hai class được định nghĩa trong file này: AutoSuggestControl vs SugguestProvider.
 * */
/**
 * An autosuggest textbox control.
 * The AutoSuggestControl class is the wrapper for all autosuggest functionality.
 * To work properly, the control needs to know which textbox to work on and the suggestion provider to use.
 * This makes for a relatively simple constructor
 * @class
 * @scope public
 */
function AutoSuggestControl(oTextbox /*:HTMLInputElement*/, oProvider /*:SuggestionProvider*/) {
    
    
    /**
     * In order for the user to use the up and down arrow keys,
     * you’ll need to keep track of the currently selected item in the suggestions list.
     * To do this, you must add two properties to the AutoSuggestControldefinition
     */
    this.cur /*:int The currently selected suggestions.*/ = -1;
    this.userText /*:String The text that the user typed.*/ = oTextbox.value;
    
    /**
     * The cur property stores the index of the current suggestion in the suggestions array.
     * By default, this value is set to -1because there are no suggestions initially.
     * When the arrow keys are pressed, cur will change to point to the current suggestion.
     * 
     * The second added property, userText, holds the current value of the textbox
     * and changes to reflect what the user actually typed.
     * */

    /**
     * The dropdown list layer.
     * Earlier in the chapter, you took a look at the HTML and CSS used for the drop-down list of suggestions.
     * Now, the task is to create the HTML programmatically and apply the CSS to create the actual functionality;
     * this is a multistep process.
     * First, a property is needed to store the <div/> element because various methods of the AutoSuggestControl
     * need access to it. This property is called layer and is initially set to null
     * @scope private
     */
    this.layer = null;
    
    /**
     * Suggestion provider for the autosuggest feature.
     * @scope private.
     */
    this.provider /*:SuggestionProvider*/ = oProvider;
    
    /**
     * The textbox to capture.
     * @scope private
     */
    this.textbox /*:HTMLInputElement*/ = oTextbox;
    
    /**
     * Timeout ID for fast typers.
     * @scope private
     */
    this.timeoutId /*:int*/ = null;
    
    //initialize the control
    this.init();
    
}

/**
 * Autosuggests one or more suggestions for what the user has typed.
 * If no suggestions are passed in, then no autosuggest occurs.
 * @scope private
 * @param aSuggestions An array of suggestion strings.
 * @param bTypeAhead If the control should provide a type ahead suggestion.
 */
AutoSuggestControl.prototype.autosuggest = function (aSuggestions /*:Array*/, bTypeAhead /*:boolean*/) {
    
    //re-initialize pointer to current suggestion
    this.cur = -1;
    
    //make sure there's at least one suggestion
    if (aSuggestions.length > 0) {
        if (bTypeAhead) {
           this.typeAhead(aSuggestions[0]);
        }
        this.showSuggestions(aSuggestions);
    } else {
        this.hideSuggestions();
    }
};

/**
 * Creates the dropdown layer to display multiple suggestions.
 * @scope private
 */
AutoSuggestControl.prototype.createDropDown = function () {
    
    //create the layer and assign styles
    //This code first creates the <div/> element and assigns it to the layer property.
    this.layer = document.createElement("div");
    
    //The className(equivalent to the class attribute) is set to suggestions, as is needed for the CSS to work properly
    this.layer.className = "suggestions";
    
    //hides the layer, since it should be invisible initially.
    this.layer.style.visibility = "hidden";
    
    //The width of the layer is set equal to the width of the textbox
    //by using the textbox’s offsetWidth property (this is optional depending on your individual needs)
    this.layer.style.width = this.textbox.offsetWidth;
    
    
    document.body.appendChild(this.layer);
    
    //when the user clicks on the a suggestion, get the text (innerHTML) and place it into a textbox
    /**
     * At this point, the only concern is making sure that the drop-down list is functional
     * if the user uses the mouse. That is, when the drop-down list is visible,
     * moving the mouse over a suggestion should highlight it.
     * Likewise, when a suggestion is clicked on, it should be placed in the textbox and the drop-down list should be hidden.
     * To make this happen, you need to assign three event handlers: onmouseover, onmousedown, and onmouseup.
     * */
    
    /**
     * The first part of this section is the assignment that makes oThisequal to the this object.
     * This is necessary so that a reference to the AutoSuggestControl object is accessible from within the event handler
     * */
    var oThis = this;
    
    /**
     * Next, a compound assignment occurs, assigning the same function
     * as an event handler for onmousedown, onmouseup, and onmouseover
     * */
    this.layer.onmousedown = this.layer.onmouseup = this.layer.onmouseover = function (oEvent) {
        
        /**
         * Inside of the function, the first two lines are used to account for the different event models (DOM and IE),
         * using a logical OR (||) to assign the values for oEventand oTarget.
         * (The target will always be a <div/> element containing a suggestion.)
         * */
        oEvent = oEvent || window.event;
        oTarget = oEvent.target || oEvent.srcElement;
        
        
        /**
         * If the event being handled is mousedown,
         * then set the value of the textbox equal to the text inside of the event target.
         * The text inside of the <div/> element is contained in a text node, which is the first child node.
         * The actual text string is contained in the text node’s nodeValue property.
         * After the suggestion is placed into the textbox, the drop-down list is hidden.
         * */
        if (oEvent.type == "mousedown") {
            oThis.textbox.value = oTarget.firstChild.nodeValue;
            oThis.hideSuggestions();
        } else if (oEvent.type == "mouseover") {
            oThis.highlightSuggestion(oTarget);
        } else {
            oThis.textbox.focus();
        }
    };
    
};

/**
 * Gets the left coordinate of the textbox.
 * @scope private
 * @return The left coordinate of the textbox in pixels.
 * 
 * The offsetLeft and offsetTop properties tell you how many pixels away from the left
 * and top of the offsetParent an element is placed.
 * The offsetParent is usually, but not always, the parent node of the element,
 * so to get the left position of the textbox, you need to add up the offsetLeft properties of the textbox
 * and all of its ancestor elements (stopping at <body/>), 
 */
AutoSuggestControl.prototype.getLeft = function () /*:int*/ {
    
    var oNode = this.textbox;
    var iLeft = 0;
    
    /**
     * Ban đầu là có thằng textbox rồi, tìm khoảng offsetLeft bên trái so với thằng cha, nều thằng cha là body,
     * lấy được tọa tộ X
     * 
     * The getLeft() method begins by pointing oNode at the textbox and defining iLeft with an initial value of 0.
     * The while loop will continue to add oNode.offsetLeft to iLeft
     * as it traverses up the DOM structure to the <body/> element.
     * */
    while(oNode != document.body) {
        iLeft += oNode.offsetLeft;
        oNode = oNode.offsetParent;
    }
    
    return iLeft;
};

/**
 * Gets the top coordinate of the textbox.
 * @scope private
 * @return The top coordinate of the textbox in pixels.
 */
AutoSuggestControl.prototype.getTop = function () /*:int*/ {

    var oNode = this.textbox;
    var iTop = 0;
    
    while(oNode != document.body) {
        iTop += oNode.offsetTop;
        oNode = oNode.offsetParent;
    }
    
    return iTop;
};

/**
 * Highlights the next or previous suggestion in the dropdown and places the suggestion into the textbox.
 * @param iDiff Either a positive (đại lượng dương) or negative (âm) number indicating
 * whether to select the next or previous sugggestion, respectively.
 * @scope private
 * 
 * As curchanges, the highlighted suggestion changes as well. To encapsulate this functionality,
 * a method called goToSuggestion()is used.
 * This method accepts only one argument, a number whose sign indicates which direction to move in.
 * For instance, any number greater than 0 moves the selection to the next suggestion;
 * any number less than or equal to 0 moves the selection to the previous suggestion
 */
AutoSuggestControl.prototype.goToSuggestion = function (iDiff /*:int*/) {
    var cSuggestionNodes = this.layer.childNodes;
    
    if (cSuggestionNodes.length > 0) {
        var oNode = null;
        
        if (iDiff > 0) {
            if (this.cur < cSuggestionNodes.length-1) {
                oNode = cSuggestionNodes[++this.cur];
            }
        } else {
            if (this.cur > 0) {
                oNode = cSuggestionNodes[--this.cur];
            }
        }
        
        if (oNode) {
            this.highlightSuggestion(oNode);
            this.textbox.value = oNode.firstChild.nodeValue;
        }
    }
};

/**
 * Handles three keydown events.
 * @scope private
 * @param oEvent The event object for the keydown event.
 * There are three events that deal with keys: keydown, keypress, and keyup.
 * The keydown event fires whenever the user presses a key on the keyboard but before any changes occur to the textbox
 * 
 * This obviously won’t help with auto-suggest because you need to know the full text of the textbox;
 * using this event would mean being one keystroke behind. For the same reason, the keypress event can’t be used.
 * It is similar to keydown but fires only when a character key is pressed.
 * 
 * The keyup event, however, fires after changes have been made to the textbox,
 * which is exactly when auto-suggest should begin working
 */
AutoSuggestControl.prototype.handleKeyDown = function (oEvent /*:Event*/) {
    /**
     * Setting up an event handler for the textbox involves two steps:
     * - defining a function
     * - assigning it as an event handler.
     * */
    switch(oEvent.keyCode) {
        case 38: //up arrow
            this.goToSuggestion(-1);
            break;
        case 40: //down arrow 
            this.goToSuggestion(1);
            break;
        case 27: //esc
            this.textbox.value = this.userText;
            this.selectRange(this.userText.length, 0);
            /* falls through */
        case 13: //enter
            this.hideSuggestions();
            oEvent.returnValue = false;
            if (oEvent.preventDefault) {
                oEvent.preventDefault();
            }
            break;
    }

};

/**
 * Handles keyup events.
 * @scope private
 * @param oEvent The event object for the keyup event.
 */
AutoSuggestControl.prototype.handleKeyUp = function (oEvent /*:Event*/) {
    
    var iKeyCode = oEvent.keyCode;
    var oThis = this;
    
    //get the currently entered text
    //Along the same lines, it’s important to set userTextto the correct value
    this.userText = this.textbox.value;
    
    /**
     * The second new line of code clears any timeout that may have already been started;
     * this cancels any suggestion request that may have been initiated.
     * */
    clearTimeout(this.timeoutId);
    
    //for backspace (8) and delete (46), shows suggestions without typeahead
    /**
     * This functionality now works exactly as it did previously,
     * but there are a couple of other keys that require special attention: Backspace and Delete.
     * When either of these keys is pressed, you don’t want to activate the typeahead functionality
     * because it will disrupt the process of removing characters from the textbox.
     * However, there’s no reason not to show the drop-down list of suggestions.
     * For the Backspace (key code of 8) and Delete (key code of 46) keys, you can call requestSuggestions(),
     * but this time, pass in falseto indicate that typeahead should not occur
     * */
    if (iKeyCode == 8 || iKeyCode == 46) {
        
        this.timeoutId = setTimeout( function () {
            oThis.provider.requestSuggestions(oThis, false);
        }, 250);
        
    //make sure not to interfere with non-character keys
    } else if ((iKeyCode != 16&& iKeyCode < 32) || (iKeyCode >= 33 && iKeyCode < 46) || (iKeyCode >= 112 && iKeyCode <= 123)) {
        //ignore loại bỏ kí tự đặt biệt
    } else {
        //request suggestions from the suggestion provider with typeahead
        
        /**
         * The other two sections of new code change the call to the requestSuggestions()to occur after 250 milliseconds
         * (which is plenty of time for this purpose).
         * Each call is wrapped in an anonymous function that is passed in to setTimeout().
         * The result of setTimeout(), the timeout ID is stored in the new property for later usage.
         * All in all, this ensures that no requests will be made unless the user has stopped typing for at least 250 milliseconds.
         * */
        this.timeoutId = setTimeout( function () {
            oThis.provider.requestSuggestions(oThis, true);
        }, 250);
        
        /**
         * The first reason is to add the bTypeAhead argument to the requestSuggestions() call.
         * When called from here, this argument will always be true:
         * Remember, the requestSuggestions() method is defined on the suggestion provider,
         * which is described later in this chapter
         * */
    }
    /**
     * Now when the user is removing characters,
     * suggestions will still be displayed and the user can click one of them to select the value for the textbox.
     * This is acceptable, but to really be usable, the autosuggest control needs to respond to keyboard controls.
     * */
};

/**
 * Hides the suggestion dropdown.
 * The drop-down list will be created after you define a few simple methods to help control its behavior.
 * The simplest method is hideSuggestions(), which hides the drop-down list after it has been shown
 * @scope private
 */
AutoSuggestControl.prototype.hideSuggestions = function () {
    this.layer.style.visibility = "hidden";
};

/**
 * Highlights the given node in the suggestions dropdown.
 * @scope private
 * @param oSuggestionNode The node representing a suggestion in the dropdown.
 * A method is needed for highlighting the current suggestion in the drop-down list.
 * The highlightSuggestion() method accepts a single argument, which is the <div/> element containing the current suggestion.
 * The purpose of this method is to set the <div/>element’s class attribute to current on the current suggestion
 * and clear the class attribute on all others in the list.
 * Doing so provides a highlighting effect on the drop-down list similar to the regular form controls.
 * The algorithm is quite simple: iterate through the child nodes of the layer.
 * If the child node is equal to the node that was passed in, set the class to current;
 * otherwise, clear the class attribute by setting it to an empty string
 */
AutoSuggestControl.prototype.highlightSuggestion = function (oSuggestionNode) {
    
    for (var i=0; i < this.layer.childNodes.length; i++) {
        var oNode = this.layer.childNodes[i];
        if (oNode == oSuggestionNode) {
            oNode.className = "current";
        } else if (oNode.className == "current") {
            oNode.className = "";
        }
    }
};

/**
 * Initializes the textbox with event handlers for
 * auto suggest functionality.
 * @scope private
 */
AutoSuggestControl.prototype.init = function () {
    
    //save a reference to this object
    var oThis = this;
    
    //assign the onkeyup event handler
    this.textbox.onkeyup = function (oEvent) {
        //check for the proper location of the event object
        if (!oEvent) { oEvent = window.event; }
        oThis.handleKeyUp(oEvent);//call the handleKeyUp() method with the event object
    };
    
    //assign onkeydown event handler
    this.textbox.onkeydown = function (oEvent) {
        if (!oEvent) { oEvent = window.event; }//check for the proper location of the event object
        oThis.handleKeyDown(oEvent);//call the handleKeyDown() method with the event object
    };
    
    //assign onblur event handler (hides suggestions)
    this.textbox.onblur = function () {
        oThis.hideSuggestions();
    };
    
    //create the suggestions dropdown
    this.createDropDown();
};

/**
 * Selects a range of text in the textbox.
 * @scope public
 * @param iStart The start index (base 0) of the selection.
 * @param iEnd The end index of the selection.
 */
AutoSuggestControl.prototype.selectRange = function (iStart /*:int*/, iEnd /*:int*/) {

    //use text ranges for Internet Explorer
    if (this.textbox.createTextRange) {
        var oRange = this.textbox.createTextRange();
        oRange.moveStart("character", iStart);
        oRange.moveEnd("character", iEnd - this.textbox.value.length);
        oRange.select();
        
    //use setSelectionRange() for Mozilla
    } else if (this.textbox.setSelectionRange) {
        this.textbox.setSelectionRange(iStart, iEnd);
    }

    //set focus back to the textbox
    this.textbox.focus();
}; 

/**
 * Builds the suggestion layer contents, moves it into position,
 * and displays the layer.
 * @scope private
 * @param aSuggestions An array of suggestions for the control.
 */
AutoSuggestControl.prototype.showSuggestions = function (aSuggestions /*:Array*/) {
    
    var oDiv = null;
    this.layer.innerHTML = "";//clear contents of the layer
    
    for (var i = 0; i < aSuggestions.length; i++) {
        oDiv = document.createElement("div");
        oDiv.appendChild(document.createTextNode(aSuggestions[i]));
        this.layer.appendChild(oDiv);
    }
    
    this.layer.style.left = this.getLeft() + "px";
    this.layer.style.top = (this.getTop()+this.textbox.offsetHeight) + "px";
    this.layer.style.visibility = "visible";
    
    /**
     * The first line simply defines the variable oDiv for later use.
     * The second line clears the contents of the drop-down list by setting the innerHTMLproperty to an empty string.
     * the forloop creates a <div/>element and a text node for each suggestion before adding it to the drop-down list layer
     * 
     * The next section of code starts by setting the left position of the layer using the getLeft() method.
     * To set the top position, you need to add the value from getTop() to the height of the textbox
     * (retrieved by using the offsetHeightproperty).
     * Without doing this, the drop-down list would appear directly over the textbox.
     * (Remember, getTop() retrieves the top of the textbox, not the top of the drop-down list layer.)
     * Last, the layer’s visibilityproperty is set to visibleto show it.
     * 
     * */
};

/**
 * Inserts a suggestion into the textbox, highlighting the suggested part of the text.
 * @scope private
 * @param sSuggestion The suggestion for the textbox.
 */
AutoSuggestControl.prototype.typeAhead = function (sSuggestion /*:String*/) {

    //check for support of typeAhead functionality
    if (this.textbox.createTextRange || this.textbox.setSelectionRange){
        var iLen = this.textbox.value.length; 
        this.textbox.value = sSuggestion; 
        this.selectRange(iLen, sSuggestion.length);
    }
};

/**
 * Provides suggestions for state/province names.
 * @class
 * @scope public
 */
function SuggestionProvider() {
    /**
     * This single instance is created using the zXML library’s zXmlHttpfactory and is stored in a property called xhr
     * */
    this.xhr = zXmlHttp.createRequest();
}

/**
 * Request suggestions for the given autosuggest control. 
 * @scope protected
 * @param oAutoSuggestControl The autosuggest control to provide suggestions for.
 */
SuggestionProvider.prototype.requestSuggestions = function (
        oAutoSuggestControl /*:AutoSuggestControl*/,
        bTypeAhead /*:boolean*/) {
    //The first line inside the method sets oXHRequal to the stored XHR object
    //This is done simply for convenience and to keep the code clean
    var oXHR = this.xhr;
    
    //cancel any active requests
    //check to make sure that there isn’t already a request waiting for a response
    //If the XHRobject is ready to be used cleanly, its readyStatewill be 0; otherwise,
    //You must cancel the existing request (by calling abort()) before making another request
    if (oXHR.readyState != 0) {
        oXHR.abort();
    }
    
    //define the data
    /**
     * Because the data being sent to the server is to be JSON-encoded, 
     * you first need to create an object (oData) to hold the information.
     * There are three pieces of information being sent: the table to get the data out of,
     * the current value in the textbox, and the maximum number of suggestions to retrieve (5).
     * The maximum number of suggestions is important because it prevents long database queries
     * from being executed repeatedly
     * */
    var oData = {
        requesting: "StatesAndProvinces",
        text: oAutoSuggestControl.userText,
        limit: 5
    };
    
    //open connection to server 
    /**
     * Next, a request is opened to suggestions.php, the server-side component of the control.
     * This request is asynchronous (last argument of open()is set to true),
     * so it’s necessary to provide an onreadystatechange event handler.
     * The event handler first checks to ensure that the readyStateis 4, 
     * and then parses the returned text as a JSON array of values.
     * This array, along with the original typeahead flag,
     * is then passed back to the AutoSuggestControl via the autosuggest() method.
     * */
    oXHR.open("post", "suggestions.jsp", true);
    oXHR.onreadystatechange = function () {
        if (oXHR.readyState == 4) {
            //evaluate the returned text JavaScript (an array)
            var aSuggestions = oXHR.responseText.parseJSON();
            
            //provide suggestions to the control
            oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);
/**
 * The last step in this method is, of course, to send the request. Note that since the request is doing a POST,
 * the data has to be passed into the send()method. The oDataobject is first encoded into JSON before being sent.
 * */
        }
    };
    
    //send the request
    oXHR.send(oData.toJSONString());
};