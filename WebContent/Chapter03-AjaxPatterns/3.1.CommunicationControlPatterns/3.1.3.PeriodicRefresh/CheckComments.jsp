<%@page import="com.utils.Constrant"%>
<%@page contentType="text/plain" pageEncoding="UTF-8" import="java.sql.*"%>
<%
  //set caching information
  /**
  Perhaps the most important parts of this file are the two headers included at the top.
  
  By setting Cachecontrol and Expiresheaders, you are telling the browser to always retrieve this file from the server and
  not from the client cache. Without this, some browsers would return the same information repeatedly,
  effectively nullifying this functionality altogether.
  The rest of this file should look very familiar,
  because it uses essentially the same algorithm as previous examples that make use of MySQL database calls.
   */
  response.addHeader("Cache-control", "No-cache");
  response.addDateHeader("Expires", 0);
  
  //database information
  String dbservername = "localhost";
  String dbname = Constrant.DB_NAME;
  String username = Constrant.DB_USERNAME;
  String password = Constrant.DB_PASSWORD;
  String url = "jdbc:mysql://" + dbservername + "/" + dbname + "?user=" + username + "&password=" + password;
  
  //query
  String sql = "SELECT CommentId, Name, LEFT(Message, 50) AS ShortMessage FROM BlogComments ORDER BY Date DESC Limit 0,1";
  
  //output message
  String message = "-1|| || ";
  
  try {
    Class.forName("com.mysql.jdbc.Driver").newInstance();
    
    //create database connection
    Connection conn = DriverManager.getConnection(url);
    
    //execute query
    Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery(sql);
    boolean found = rs.next();
    
    //if there was a match...
    if (found) {
      message = rs.getString("CommentId") + "||" + rs.getString("Name") + "||" + rs.getString("ShortMessage");
    }
    
    rs.close();
    conn.close();
  } catch (Exception ex) {
    //ignore
  }
%><%=message%>