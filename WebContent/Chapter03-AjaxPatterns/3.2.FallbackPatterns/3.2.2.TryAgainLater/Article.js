/**
 * To track the number of failed attempts, a global variable is necessary
 * */
var iFailed = 0;

/**
 * The iFailedvariable starts at 0and is incremented every time a request fails.
 * So, if iFailedis ever greater than a specific number,
 * you can just cancel the request because it is clearly not going to work
 * */
function downloadLinks() {
  var oXHR = zXmlHttp.createRequest();
  
  if (iFailed < 10) {//TODO ------- chu y
    try {
      oXHR.open("get", "AdditionalLinks.txt", true);
      oXHR.onreadystatechange = function() {
        if (oXHR.readyState == 4) {
          if (oXHR.status == 200 || oXHR.status == 304) {
            var divAdditionalLinks = document.getElementById("divAdditionalLinks");
            divAdditionalLinks.innerHTML = oXHR.responseText;
            divAdditionalLinks.style.display = "block";
          } else {//TODO ------- chu y
            iFailed++;
            downloadLinks();
            //throw new Error("An error occurred.");
          }
        }
      };
      
      oXHR.send(null);
    } catch (oException) {//TODO ------- chu y
      iFailed++;
      downloadLinks();
    }
  }
}

window.onload = function() {
  if (zXmlHttp.isSupported()) {
    downloadLinks();
  }
};