<%@page import="com.utils.Constrant"%>
<%@page contentType="text/plain" pageEncoding="UTF-8" import="java.sql.*"%>
<%
  //set caching information
  response.addHeader("Cache-control", "No-cache");
  response.addDateHeader("Expires", 0);
  
  //database information
  String dbservername = "localhost";
  String dbname = Constrant.DB_NAME;
  String username = Constrant.DB_USERNAME;
  String password = Constrant.DB_PASSWORD;
  String url = "jdbc:mysql://" + dbservername + "/" + dbname + "?user=" + username + "&password=" + password;
  
  //query
  /**
  The LIMIT clause can be used to constrain the number of rows returned by the SELECT statement.
  LIMIT takes one or two numeric arguments,
  which must both be nonnegative integer constants (except when using prepared statements).
  With two arguments, the first argument specifies the offset of the first row to return,
  and the second specifies the maximum number of rows to return. The offset of the initial row is 0 (not 1):
  
      sắp xếp ngày theo thứ tự giảm dần, lấy ra max ngày, ngày comment gàn nhất. iLastComment
   */
  String sql = "SELECT CommentId, Name, LEFT(Message, 50) AS ShortMessage FROM BlogComments ORDER BY Date DESC Limit 0,1";
  
  //output message
  String message = "-1|| || ";
  
  try {
    Class.forName("com.mysql.jdbc.Driver").newInstance();
    
    //create database connection
    Connection conn = DriverManager.getConnection(url);
    
    //execute query
    Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery(sql);
    boolean found = rs.next();
    
    //if there was a match...
    if (found) {
      message = rs.getString("CommentId") + "||" + rs.getString("Name") + "||" + rs.getString("ShortMessage");
    }
    
    rs.close();
    conn.close();
  } catch (Exception ex) {
    //ignore
  }
%><%=message%>