<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>jQuery load() POST Example</title>
    <script type="text/javascript"src="jquery.js"></script>
    <script type="text/javascript">
        function sendRequest() {
            var oForm = document.forms[0];
            var oBody = getRequestBody(oForm);
            
            //send the request
            $("div#divStatus").load("SaveCustomer.jsp", oBody);
        }
        
        function getRequestBody(oForm) {
            
            //object to hold params
            var oParams = {};
            
            //iterate over each element in the form
            for (var i=0 ; i < oForm.elements.length; i++) {
            
                //get reference to the field
                var oField = oForm.elements[i];
                
                //different behavior based on the type of field
                switch (oField.type) {
                
                    //buttons - we don't care
                    case "button":
                    case "submit":
                    case "reset":
            		         break;
                    
                    //checkboxes/radio buttons - only return the value if the control is checked.
                    case "radio":
                    case "checkbox":
                        if (!oField.checked) {
                            break;
                        } //End: if
                    
                    //text/hidden/password all return the value
                    case "text":
                    case "hidden":
                    case "password":
                        oParams[oField.name] = oField.value;
                        break;
                    
                    //everything else
                    default:
                    
                        switch(oField.tagName.toLowerCase()) {
                            case "select":
                                oParams[oField.name] = oField.options[oField.selectedIndex].value;
                                break;
                            default:	
                                oParams[oField.name] = oField.value;
                        }
                }
            
            }
            
            return oParams;
        }
        
    </script>
</head>
<body>
    <form method="post" action="SaveCustomer.aspx" onsubmit="sendRequest(); return false">
        <p>Enter customer information to be saved:</p>
        <p>Customer Name: <input type="text" name="txtName" value="" /><br />
        Address: <input type="text" name="txtAddress" value="" /><br />
        City: <input type="text" name="txtCity" value="" /><br />
        State: <input type="text" name="txtState" value="" /><br />
        Zip Code: <input type="text" name="txtZipCode" value="" /><br />
        Phone: <input type="text" name="txtPhone" value="" /><br />
        E-mail: <input type="text" name="txtEmail" value="" /></p>
        <p><input type="submit" value="Save Customer Info" /></p>
    </form>
    <div id="divStatus"></div>
</body>
</html>
