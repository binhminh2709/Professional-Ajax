<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>jQuery ajaxStart() Example</title>
    <script type="text/javascript"src="jquery.js"></script>
    
    
    <script type="text/javascript">
/**
Prototype isn’t the only library that has global event handlers for Ajax requests;
jQuery supports similar functionality by using the ajaxStart() and ajaxStop() methods.
The ajaxStart() method fires when there are no Ajax requests pending (chưa giải quyết) and one is started.
Likewise, the ajaxStop() method is called when all Ajax requests have completed.
Both methods accept a function that should be called when the event occurs.

The first step to using these methods is to retrieve a reference to an element.
Then, the ajaxStart() and ajaxStop() methods can be called on that element.
For instance, to use a <div/> element with an ID of divStatus,
*/
        $(document).ready(function () {
            $("div#divStatus").ajaxStart(function () {
                $(this).html("Contacting the server...");
                
            }).ajaxStop(function () {
                $(this).html("Response received.");
            });
        });
        
        //function to get customer information
        function requestCustomerInfo() {
        
            //get the ID
            var sId = $("input#txtCustomerId").val();
            
            //send the request
            $("div#divCustomerInfo").load("GetCustomerData.jsp?id=" + sId);
        }
        
    </script>
</head>
<body>
    <p>Enter customer ID number to retrieve information:</p>
    <p>Customer ID: <input type="text" id="txtCustomerId" value="" /></p>
    <p><input type="button" value="Get Customer Info" onclick="requestCustomerInfo()" /></p>
    <div id="divStatus" style="color: blue"></div>
    <div id="divCustomerInfo"></div>
</body>
</html>
