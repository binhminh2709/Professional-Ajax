<%@page contentType="text/plain"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<% 

    //get information (replace apostrophes with double apostrophes to prevent SQL injection attacks)
    String name = request.getParameter("txtName").replace("'", "''");
    String address = request.getParameter("txtAddress").replace("'", "''");
    String city = request.getParameter("txtCity").replace("'", "''");
    String state = request.getParameter("txtState").replace("'", "''");
    String zipCode = request.getParameter("txtZipCode").replace("'", "''");
    String phone = request.getParameter("txtPhone").replace("'", "''");
    String email = request.getParameter("txtEmail").replace("'", "''");                                    

    //the message to send back
    String message = "";

    //database information
    String dbservername = "localhost";
    String dbname = "ProAjax";
    String username = "root";
    String password = "0000";
    String url = "jdbc:mysql://" + dbservername + "/" + dbname + "?user=" + username + "&password=" + password;

    try {
        
        //create instance of the MySQL driver
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        
        //create database connection
        Connection conn = DriverManager.getConnection(url);
        
        //construct the query
        StringBuffer buffer = new StringBuffer();
        buffer.append("Insert into Customers(Name,Address,City,State,Zip,Phone,Email) values ('");
        buffer.append(name);
        buffer.append("','");
        buffer.append(address);
        buffer.append("','");
        buffer.append(city);
        buffer.append("','");
        buffer.append(state);
        buffer.append("','");
        buffer.append(zipCode);
        buffer.append("','");
        buffer.append(phone);
        buffer.append("','");
        buffer.append(email);
        buffer.append("');");        
        String sql = buffer.toString();
            
        //execute it
        Statement stmt = conn.createStatement();
        if (stmt.executeUpdate(sql) > 0) {
            message = "Added customer";
        } else {
            message = "Error occurred while trying to connect to database.";
        }
        
    } catch (Exception ex){
        message = "Error occurred while trying to connect to database: " + ex.getMessage();
    }        
%><%=message%>
